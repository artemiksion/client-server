use std::{fs, time::Duration, thread};

pub fn input(input_file: &str, lines_num: &usize){
    loop{
        if let Ok(s) = fs::read_to_string(input_file){
            let string_vec = s.lines().collect::<Vec<&str>>();
            if *lines_num != string_vec.len() {
                println!("{}", string_vec[string_vec.len() - 1]);
                break;
            }
        }
        thread::sleep(Duration::from_millis(50));
    }
}