use std::{io, io::Write, time::Duration, fs::OpenOptions, io::BufRead, thread};
use chrono::prelude::*;

pub fn output(output_file: &str) {// Подумать над задержками, чтобы другая программа могла считывать

    let mut message:String = String::new();
    io::stdin().lock().read_line(&mut message).unwrap();

    let message = format!("{} [{}]\n", message.trim(), Local::now().to_rfc3339());

    loop{
        if let Ok(mut f) = OpenOptions::new().append(true).open(output_file) {
            f.write(&message.as_bytes()).unwrap();//   По идее файл в таком случае получилось открыть и записать строку в него не должно составить труда
            break;
        }
        thread::sleep(Duration::from_millis(50));
        println!("Не получается открыть файл на запись(((");
    }
}//