use std::{fs, thread, time::Duration};
//use std::sync::mpsc::{Sender, Receiver};
//use std::sync::mpsc;
//use std::process::Command;

mod output;
mod input;

fn main() {

    let mut output_file = String::new();
    let mut input_file = String::new();

    println!("Write output file:");
    std::io::stdin().read_line(&mut output_file).unwrap();
    println!("Write input file:");
    std::io::stdin().read_line(&mut input_file).unwrap();

    thread::spawn(move | |{
        loop{
            output::output(&output_file.trim());
        }
    });

    thread::spawn(move | |{
        let mut lines_num:usize = 0;
        loop{
            input::input(&input_file.trim(), &lines_num);
            lines_num += 1;
        }
    });

    loop{
        if fs::File::open("X.txt").is_ok() {
            break;
        }
        thread::sleep(Duration::from_millis(50));
    }
    println!("Программа закрыта!");
}
